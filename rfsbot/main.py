# SPDX-FileCopyrightText: 2022 Agathe Porte <debian@microjoe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import asyncio
import re

from pyrcb2 import IRCBot, Event, Reply, Error, ANY


TEST_SETTINGS = {
    "server": "irc.oftc.net",
    "port": 6667,
    "channel": "#auie",
    "nick": "rfsbot_test",
}

PROD_SETTINGS = {
    "server": "irc.oftc.net",
    "port": 6667,
    "channel": "#debian-python",
    "nick": "rfsbot",
}

# TODO: Change when in production to PROD_SETTINGS instead of TEST_SETTINGS
SETTINGS = TEST_SETTINGS


RFS_MATCH = re.compile(r"^(.*) RFS \[(.*)\]\s*$")


class MyBot:
    def __init__(self):
        self.bot = IRCBot(log_communication=True)
        self.bot.load_events(self)

    async def run(self):
        async def init():
            await self.bot.connect(SETTINGS["server"], SETTINGS["port"])
            await self.bot.register(SETTINGS["nick"])
            await self.bot.join(SETTINGS["channel"])

        await self.bot.run(init())

    async def get_topic(self, channel):
        future = self.bot.send_command("TOPIC", channel)
        result = await self.bot.wait_for(
            future,
            capture=Reply("RPL_TOPIC", channel, ANY),
        )
        if result.success:
            try:
                result.value = result.messages[0].args[2]
            except IndexError:
                # No topic is set
                result.value = ""

        return result

    async def set_topic(self, channel, topic):
        future = self.bot.send_command("TOPIC", channel, topic)
        return await self.bot.wait_for(
            future,
            capture=Reply("RPL_TOPIC", channel, topic),
            errors=[
                Error("ERR_CHANOPRIVSNEEDED", channel, ANY),
            ],
        )

    @Event.privmsg
    async def on_privmsg(self, sender, channel, message):
        if channel is None:
            # Message was sent in a private query.
            from importlib import metadata
            self.bot.privmsg(sender, metadata.version(__package__))
            return

        # Message was sent in a channel.
        if channel != SETTINGS["channel"]:
            return

        words = message.split(" ")
        if words[0] != "!rfs":
            return

        if len(words) == 1:
            self.bot.privmsg(channel, "usage: !rfs <package1> [<package2>...]")
            return

        topic = await self.get_topic(channel)
        if not topic.success:
            self.bot.privmsg(channel, "Could not get topic.")
            return

        topic = topic.value
        match = RFS_MATCH.match(topic)
        if not match:
            self.bot.privmsg(channel, "Could not parse topic.")
            return

        left = match.group(1)
        packages = match.group(2).split(",")
        packages = [p.strip() for p in filter(lambda p: p != "", packages)]

        for p in filter(lambda p: p != "", words[1:]):
            if p in packages:
                packages.remove(p)
            else:
                packages.append(p)

        plist = ", ".join(packages)
        res = await self.set_topic(channel, f"{left} RFS [{plist}]")
        if not res.success:
            self.bot.privmsg(channel, "Could not set topic.")
            return


async def amain():
    """Async main."""
    mybot = MyBot()
    await mybot.run()


def main():
    """Main."""
    asyncio.run(amain())


if __name__ == "__main__":
    main()
