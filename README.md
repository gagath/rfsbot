<!--
SPDX-FileCopyrightText: 2022 Agathe Porte <debian@microjoe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# rfsbot

An IRC bot for managing Debian RFS in IRC topics.

## Usage

When the bot is started, the following commands can be used.

### !rfs

Adds (or remove) an item from the RFS list in the corresponding IRC channel’s
topic:

- If the item *is not in the list*, it will be *added*.
- If the item *is in the list*, it will be *removed*.

```
# topic is now set to "foobar RFS []"
!rfs rfsbot
# topic is now set to "foobar RFS [rfsbot]"
!rfs rfsbot
# topic is now set to "foobar RFS []"
```

## License

GNU General Public License v3.0.
