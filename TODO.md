- [ ] Use production settings when called by systemd, using argv
- [ ] Use the package instead of manual run when bookworm is released with
      `ptyhon3-pyrcb` package inside.
